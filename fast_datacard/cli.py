# -*- coding: utf-8 -*-

"""Console script for fast-datacard."""
import sys
import click
from . import dataframe_to_datacard_cfg


@click.command()
@click.argument('yaml_config')
@click.option('-o', '--outdir', default=None, help='specify custom outdir')
@click.option('-i', '--in_dir', default=None, help='specify custom dir with pre-datacard inputs')
@click.option('-q', '--query', default=None, help='query to apply before datacard production')
@click.option('-k', '--keep', is_flag=True, help='Keep pre-existing datacards in output dir')
def main(yaml_config, outdir, in_dir, query, keep):
    """Console script for fast-datacard."""
    return dataframe_to_datacard_cfg.main(yaml_config, outdir, in_dir, query, keep)


if __name__ == "__main__":
    sys.exit(main())  # pragma: no cover
